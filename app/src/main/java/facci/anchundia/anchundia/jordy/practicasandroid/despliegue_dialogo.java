package facci.anchundia.anchundia.jordy.practicasandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class despliegue_dialogo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_despliegue_dialogo);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu ,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;

        switch (item.getItemId()){
            case R.id.opcionLogin:
                Dialog dialogoLogin = new Dialog(despliegue_dialogo.this);
                dialogoLogin.setContentView(R.layout.dlg_log);

                Button btnautenticar = (Button) dialogoLogin.findViewById(R.id.btnautenticar);
                final EditText Cajauser = (EditText) dialogoLogin.findViewById(R.id.txtuser);
                final EditText Cajaclave = (EditText) dialogoLogin.findViewById(R.id.txtclave);

                btnautenticar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(despliegue_dialogo.this,"Usuario: "+
                                Cajauser.getText().toString()+"  Clave: "+Cajaclave.getText().toString()
                                ,Toast.LENGTH_LONG).show();
                    }
                });
                dialogoLogin.show();
                break;
            case R.id.opcionRegistrar:
                Dialog dialogoRegistrar = new Dialog(despliegue_dialogo.this);
                dialogoRegistrar.setContentView(R.layout.dlg_registro);
                Button btnregistrar=(Button)dialogoRegistrar.findViewById(R.id.btnregistrar);
                final EditText cajanombre=(EditText)dialogoRegistrar.findViewById(R.id.txtnombre);
                final EditText cajapellido=(EditText)dialogoRegistrar.findViewById(R.id.txtapellidos);

                btnregistrar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(despliegue_dialogo.this, "Nombres: "
                                + cajanombre.getText().toString() + "  Apellidos: " +
                                cajapellido.getText().toString(), Toast.LENGTH_LONG).show();
                    }
                });
                dialogoRegistrar.show();
                break;

        }

        return true;
    }
}
