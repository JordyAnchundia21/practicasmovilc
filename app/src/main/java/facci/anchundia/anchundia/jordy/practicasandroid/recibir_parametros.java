package facci.anchundia.anchundia.jordy.practicasandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class recibir_parametros extends AppCompatActivity {
    TextView txtresultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibir_parametros);
        txtresultado = (TextView)findViewById(R.id.txtresultado);
        Bundle bundle = this.getIntent().getExtras();
        txtresultado.setText(bundle.getString("dato"));
    }
}
