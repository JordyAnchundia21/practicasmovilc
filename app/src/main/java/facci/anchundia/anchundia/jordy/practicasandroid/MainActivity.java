package facci.anchundia.anchundia.jordy.practicasandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button buttonlogin, buttonregistrar, buttonbuscar, buttonparametro,
            buttonFragmento, buttonAutenticar, buttonSensores;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonlogin=  (Button) findViewById(R.id.buttonLogin);
        buttonregistrar=  (Button) findViewById(R.id.buttonRegistar);
        buttonbuscar=  (Button) findViewById(R.id.buttonBuscar);
        buttonparametro=  (Button) findViewById(R.id.buttonparametro);
        buttonFragmento = (Button) findViewById(R.id.buttonFragmento);
        buttonlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent  intent = new Intent(MainActivity.this, login.class);
                startActivity(intent);
            }
        });
        buttonbuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent  intent = new Intent(MainActivity.this, buscar.class);
                startActivity(intent);
            }
        });
        buttonregistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent  intent = new Intent(MainActivity.this, registrar.class);
                startActivity(intent);
            }
        });
        buttonparametro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, pasar_parametros.class);
                startActivity(intent);

            }
        });
        buttonFragmento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Fragmentos.class);
                startActivity(intent);

            }
        });

    }
}
